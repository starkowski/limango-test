<?php

namespace App\Entity;

class Coin
{
    private array $availableValues = [2,1,0.50,0.20,0.10,0.05,0.02,0.01];

    public function getAvailableCoins(): array
    {
        return $this->availableValues;
    }
}
