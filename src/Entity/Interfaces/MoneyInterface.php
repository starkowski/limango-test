<?php

namespace App\Entity\Interfaces;

interface MoneyInterface
{
    public function getAmount(): int;
    public function setAmount(int $amount): self;
}