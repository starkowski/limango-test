<?php

namespace App\Entity;

use App\Entity\Interfaces\MoneyInterface;

class Money implements MoneyInterface
{
    private int $amount;

    public function getAmount(): int
    {
        return $this->amount;
    }

    public function setAmount(int $amount): self
    {
        $this->amount = $amount;

        return $this;
    }
}
