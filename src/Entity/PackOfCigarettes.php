<?php

namespace App\Entity;

class PackOfCigarettes
{
    public const STATIC_PRICE = 499;

    private int $amount;

    public function getAmount(): int
    {
        return $this->amount;
    }

    public function setAmount(int $amount): PackOfCigarettes
    {
        $this->amount = $amount;

        return $this;
    }

    public function getPackPrice(): int
    {
        return self::STATIC_PRICE;
    }
}
