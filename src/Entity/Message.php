<?php

namespace App\Entity;

class Message
{
    public const MESSAGE_SUCCESS = "You bought %d packs of cigarettes for %01.2f€, each for %01.2f€.\n";
    public const MESSAGE_PAYMENT_NOT_SUFFICIENT = "The entered amount of %01.2f€ is too low. To make a purchase of %d packs you have to pay %01.2f€.\n";

    private string $info;
    private array $change;

    public function getInfo(): string
    {
        return $this->info;
    }

    public function setInfo(string $info): Message
    {
        $this->info = $info;
        return $this;
    }

    public function getChange(): array
    {
        return $this->change;
    }

    public function setChange(array $change): Message
    {
        $this->change = $change;
        return $this;
    }
}
