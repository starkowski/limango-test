<?php

namespace App\Command;

use App\Service\PurchaseService;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class PurchaseCigarettesCommand extends Command
{
    protected static $defaultName = 'purchase-cigarettes';
    protected static $defaultDescription = 'A small CLI cigarette machine.';

    private $purchaseService;

    public function __construct(PurchaseService $purchaseService)
    {
        $this->purchaseService = $purchaseService;

        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->addArgument(
                'packsAmount', InputArgument::REQUIRED,
                'The number of packages you want to order.'
            )
            ->addArgument(
                'moneyAmount', InputArgument::REQUIRED,
                'The amount of money entered.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $purchase = $this->purchaseService
            ->setPacksOfCigarettes(
                $input->getArgument('packsAmount')
            )
            ->setPayment(
                $input->getArgument('moneyAmount')
            );

        $purchase->execute();

        $io->text($purchase->getMessage()->getInfo());

        if ( $purchase->getMessage()->getChange() )
        {
            $io->text("Your change is:");

            $io->table(
                ['coins','count'],
                $purchase->getMessage()->getChange()
            );
        }

        return Command::SUCCESS;
    }
}
