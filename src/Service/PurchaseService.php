<?php

namespace App\Service;

use App\Entity\Change;
use App\Entity\Coin;
use App\Entity\Interfaces\MoneyInterface;
use App\Entity\Message;
use App\Entity\DueCharge;
use App\Entity\PackOfCigarettes;
use App\Entity\Payment;

class PurchaseService
{
    public Message $output;

    private Payment $payment;
    private DueCharge $dueCharge;
    private Change $change;
    private Message $message;
    private PackOfCigarettes $packOfCigarettes;

    private array $changeCoins;

    public function execute(): void
    {
        $this->dueCharge = new DueCharge();
        $this->dueCharge->setAmount(
            $this->calculateDueCharge()
        );

        $this->checkIfPaymentAmountIsSufficient()
            ? $this->executeSuccess()
            : $this->executeFailure();
    }

    private function executeSuccess(): void
    {
        $this->prepareSuccessMessage();
        $this->calculateChange();
        $this->prepareCoinsForChange(
            $this->change
        );
    }

    private function executeFailure(): void
    {
        $this->prepareFailureMessage();
        $this->prepareCoinsForChange(
            $this->payment
        );
    }

    private function calculateDueCharge()
    {
        return $this->packOfCigarettes->getAmount() * ($this->packOfCigarettes->getPackPrice());
    }

    private function checkIfPaymentAmountIsSufficient()
    {
        return  $this->payment->getAmount() >= $this->dueCharge->getAmount();
    }

    private function calculateChange()
    {
        $this->change = new Change();
        $this->change->setAmount(
            $this->payment->getAmount() - $this->dueCharge->getAmount()
        );
    }

    private function prepareCoinsForChange(MoneyInterface $money)
    {
        $coins = (new Coin())
            ->getAvailableCoins();
        arsort($coins);

        $this->changeCoins = [];
        $changeCheckAmount = 0;
        foreach ($coins as $key => $coinValue) {
            $coinValueMultiplied = $coinValue * 100;
            while ($changeCheckAmount + $coinValueMultiplied <= $money->getAmount()) {
                $this->changeCoins[$coinValueMultiplied] = isset($this->changeCoins[$coinValueMultiplied])
                    ? ++$this->changeCoins[$coinValueMultiplied]
                    : 1;
                $changeCheckAmount += $coinValueMultiplied;
            }
        }
        $this->message->setChange(
            $this->prepareIOTableFormat()
        );
    }

    private function prepareIOTableFormat()
    {
        $ioTableFormat = [];
        foreach ($this->changeCoins ?? [] as $coinValue => $coinAmount) {
            $ioTableFormat[] = [$coinValue/100,$coinAmount];
        }

        return $ioTableFormat;
    }

    public function setPayment(string $amount): self
    {
        $this->payment = new Payment();
        $this->payment->setAmount(
            round(($this->fixPaymentString($amount) * 100), 0)
        );

        return $this;
    }

    public function setPacksOfCigarettes(string $amount): self
    {
        $this->packOfCigarettes = new PackOfCigarettes();
        $this->packOfCigarettes->setAmount(
            abs($amount)
        );

        return $this;
    }

    private function fixPaymentString(string $string)
    {
        return abs(str_replace(',','.',$string));
    }

    private function prepareSuccessMessage()
    {
        $this->message = new Message();
        $this->message
            ->setInfo(
                sprintf(
                    Message::MESSAGE_SUCCESS,
                    $this->packOfCigarettes->getAmount(),
                    $this->dueCharge->getAmount()/100,
                    $this->packOfCigarettes->getPackPrice()/100
                )
            )
        ;
    }

    private function prepareFailureMessage()
    {
        $this->message = new Message();
        $this->message
            ->setInfo(
                sprintf(
                    Message::MESSAGE_PAYMENT_NOT_SUFFICIENT,
                    $this->payment->getAmount()/100,
                    $this->packOfCigarettes->getAmount(),
                    $this->dueCharge->getAmount()/100
                )
            )
        ;
    }

    public function getMessage(): Message
    {
        return $this->message;
    }
}
